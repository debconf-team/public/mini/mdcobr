#!/usr/bin/env python3

from dataclasses import dataclass
from pathlib import Path
from shutil import rmtree
from subprocess import check_call
from typing import List
from xml.sax.saxutils import escape as xml_escape

from yaml import safe_load


SLIDES = ('opening', 'apology', 'closing')
HEAD = """\
<!doctype html>
<html>
<head>
  <title>Title Previews</title>
</head>
<body>
  <table>
"""
FOOT = """\
  </table>
</body>
</html>
"""


@dataclass
class Talk:
    speakers: List[str]
    room: str
    title: str
    subtitle: str
    date: str
    apology: str


def process_template(slide, talk, output):
    with open(f'{slide}.svg') as f:
        template = f.read()

    if len(talk.speakers) > 2:
        comma_names = ', '.join(talk.speakers[:-1])
        speakers = f'{comma_names} and {talk.speakers[-1]}'
    else:
        speakers = ' and '.join(talk.speakers)

    svg_path = output.with_suffix('.svg')
    with svg_path.open('w') as f:
        f.write(template
            .replace('@SPEAKERS@', xml_escape(speakers))
            .replace('@ROOM@', xml_escape(talk.room))
            .replace('@TITLE@', xml_escape(talk.title))
            .replace('@SUBTITLE@', xml_escape(talk.subtitle))
            .replace('@DATE@', xml_escape(str(talk.date)))
            .replace('@APOLOGY@', xml_escape(talk.apology))
        )

    check_call(('inkscape', '--batch-process', '-o', output, svg_path))
    svg_path.unlink()


with open('test-data.yml') as f:
    talks = safe_load(f)

talks = [Talk(**talk) for talk in talks]

output = Path('output')
if output.exists():
    rmtree(output)
output.mkdir()

with output.joinpath('index.html').open('w') as html:
    html.write(HEAD)
    for i, talk in enumerate(talks):
        html.write('    <tr>\n')
        for slide in SLIDES:
            rendered = output.joinpath(f'{i}-{slide}.png')
            process_template(slide, talk, rendered)
            html.write('      <td>\n')
            html.write(f'        <img src="{rendered.name}">\n')
            html.write('      </td>\n')
        html.write('    </tr>\n')
    html.write(FOOT)

